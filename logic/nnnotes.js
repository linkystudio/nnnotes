var express = require('express');
var r = require('rethinkdb');
var app = express();
var bodyParser = require('body-parser');

var connection;

var ACTIONS = {
  'list': actionList,
  'post': actionPost,
}

r.connect({host:'localhost', db: 'Nnnotes', port:6000}, (err, con) => {
  if(err) console.log(err);
  else {
    connection = con;
    console.log("Connected.");
  }
})

app.use('/notes',express.static('../html'));
app.use(bodyParser())
app.post('/notes', (req, res) => {
  var body = req.body;
  var action = body.action;
  if(action in ACTIONS)ACTIONS[action](req, res, body);
  console.log(req.body)
})

app.listen(3000);

function actionList(req, res, body) {
  var before = typeof body.date === 'number' ?body.date : false;
  var q = r.table("Notes").orderBy(r.desc('r_date'));
  if(before) q = q.filter(r.row('date').lt(before));
  q = q.limit(20).coerceTo('array');
  q.run(connection, (err, values) => {
    if(err) {
      console.log(err);
      res.json({err:true})
    } else
    res.json(values);
  })
}

function actionPost(req, res, body) {
  if(typeof body.body === "string" && body.body.length > 0 && body.body.length < 1000) {
    r.table("Notes").insert({
      date: +new Date(),
      r_date:r.now(),
      body: body.body
    }).run(connection, (err, inserted) => {
      console.log(err||inserted);
      res.send({})
    })
  }
}
