var CACHE_NAME = 'my-site-cache-v1';
var urlsToCache = [
  '/'
]

self.addEventListener('install', event => {
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(cache => {
        //console.log("Opened PWA cache");
        return cache.addAll(urlsToCache);
      }).catch(e=>{
        //console.log(e)
      })
  )
})

self.addEventListener('fetch', event => {
  event.respondWith(
    caches.match(event.request)
      .then(response => {
        if(response)
          return response;
        return fetch(event.request)
      })
  )
})

self.addEventListener('activate', event => {
  var cacheWhitelist = [];
  event.waitUntil(
    caches.keys().then(cacheNames => {
      return Promise.all(
        cacheNames.map(cacheName => {
          if(cacheWhitelist.indexOf(cacheName) === -1) {
            return caches.delete(cacheName);
          }
        })
      )
    })
  )
})
